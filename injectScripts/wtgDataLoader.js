//mediaPlayerDataLoader
const ssGGUtils = {
    start: function () {
        this.running = true;
        console.log("[SS-GG Utils] Successfully injected!")
        this.loadFrame();
    },
    iframe: undefined,
    loadFrame: function () {
        if (!this.running) return;
        console.info("[SS-GG Utils] Loading iframe... please wait...")
        const iframe = document.getElementsByTagName("iframe")[0];
        if (iframe===null || iframe===undefined){
            console.warn("[SS-GG Utils] iframe not found... looking for iframe... please wait...")
            this.timer = setTimeout(()=>{
                this.loadFrame()
            }, 2000);
            return;
        }
        console.info("[SS-GG Utils] iframe found and loaded!")
        this.iframe = iframe;
        this.update();
    },
    update: function () {
        if (!this.running) return;
        //update data
        if (this.iframe.src.includes("w.soundcloud.com/player")){
            this.handleSoundCloud();
        }else{
            this.timer = setTimeout(()=>{
                this.update()
            }, 2000);
        }

    },
    running: true,
    timer: undefined,
    stop: function () {
        this.running = false;
        if (this.timer !== undefined) clearTimeout(this.timer);
    },
    handleSoundCloud: function () {
        this.scAPI = SC.Widget(this.iframe);
        this.scAPI.getCurrentSound((data)=>{
            window.postMessage({
                TYPE: "UPDATE_DATA", NEW_DATA: {
                    title: data.title,
                    author: data.publisher_metadata.artist
                }
            })
            this.timer = setTimeout(()=>{
                this.update()
            }, 2000);
        })

    },
    scAPI: undefined,
    messageListener: function () {
        window.addEventListener("message", (event) => {
            if (event.data.TYPE && (event.data.TYPE === "STOP")) {
                switch (event.data.TYPE) {
                    case "STOP":
                        this.stop();
                        break;
                    case "START":
                        this.update();
                        break
                }

            }
        })
    }
}

ssGGUtils.start();
ssGGUtils.messageListener();