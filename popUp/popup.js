class SteelSeries {
    constructor(url) {
        this.url = url;
    }

    setScreen(value) {
        this.screen = value;
    }

    getScreen() {
        return this.screen;
    }

    setURL(url) {
        this.url = url;
    }

    //{type: "TEXT", LINE: 0}
    //{type: "PROGRESS", LINE: 0}
    setText(line1, line2) {
        console.info("LINE!")
        this.sendData("game_event", {
            "game": "SSGG_WEB_UTILS",
            "event": "SC_IDLE_STATE",
            "data": {
                "value": Math.floor(Math.random() * Math.floor(99)),
                "frame": {
                    "first-line": line1,
                    "second-line": line2,
                }
            }
        }).then((data) => {
            console.log(data)
        });
    }

    setProgress(line1, progress) {

    }

    updateScreen() {
        this.sendData();
    }

    async sendData(endPoint, obj) {

// Default options are marked with *
        const response = await fetch(this.url + endPoint, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            // mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(obj) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

    registerApp() {
        this.sendData("game_metadata", {
            "game": "SSGG_WEB_UTILS",
            "game_display_name": "SteelSeriesGG WebUtils",
            "developer": "Unbemerkt"
        });
        this.sendData("bind_game_event", {
            "game": "SSGG_WEB_UTILS",
            "event": "SC_PROGRESS_UPDATE",
            "handlers": [
                {
                    "device-type": this.screen,
                    "zone": "one",
                    "mode": "screen",
                    "datas": [
                        {
                            "lines": [
                                {
                                    "has-text": true,
                                    "context-frame-key": "first-line",
                                },
                                {
                                    "has-progress-bar": true
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        this.sendData("bind_game_event", {
            "game": "SSGG_WEB_UTILS",
            "event": "SC_IDLE_STATE",
            "handlers": [
                {
                    "device-type": this.screen,
                    "zone": "one",
                    "mode": "screen",
                    "datas": [
                        {
                            "lines": [
                                {
                                    "has-text": true,
                                    "context-frame-key": "first-line",
                                },
                                {
                                    "has-text": true,
                                    "has-progress-bar": false,
                                    "context-frame-key": "second-line",
                                    "icon-id": 23
                                }
                            ]
                        }
                    ]
                }
            ]
        })
    }

    unregisterApp() {
        this.sendData("remove_game", {"game": "SSGG_WEB_UTILS"})
    }
}
var data;
var defaultConfig = {
    active: true,
    autoActive: true,
    screenSize: "screened-128x40",
    url: "none",
    selectNew: true,
    youTube: {
        active: true,
        states: {
            _n1: {
                friendlyName: "Not Started",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _0: {
                friendlyName: "Ended",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _1: {
                friendlyName: "Playing",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _2: {
                friendlyName: "Paused",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _3: {
                friendlyName: "Buffering",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _5: {
                friendlyName: "Video cued",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _6: {
                friendlyName: "No video selected",
                line1: "No Video",
                line2: "Selected!",
                line2_progress: false
            }
        }

    },
    soundCloud: {
        active: true,
        line1: "%title%",
        line2: "%artist%",
        line2_progress: false
    },
    soundCloudEmbed: {
        active: true,
        paused: {
            line1: "%title%",
            line2: "%paused%",
            line2_progress: false
        },
        line1: "%title%",
        line2: "%playing%",
        line2_progress: false
    },
    wtg: {
        active: true,
        paused: {
            line1: "» %title% «» %artist% «",
            line2: "%paused%",
            showProgress: false
        },
        line1: "» %title% «» %artist% «",
        line2: "%playing%",
        line2_progress: true
    },
    disabledTabs: []
};
var currentTabId;
var steelSeries;
//clickListener
function listenForClicks() {
    document.addEventListener("click", (e) => {
        const func = e.target.getAttribute("data-onClick");
        if (func!==null&&func!==undefined){
            window[e.target.getAttribute("data-onClick")](e.target);
        }
    });
}


function toggleActive(){
    if (data===undefined){
        console.error("DATA UNDEFINED!")
        return;
    }
    console.log("toggleActive")
    console.log(data);
    data.active = !data.active;
    browser.storage.local.set({
        "ssGG": data
    });
    updateElements();
}

function toggleAutoActive(){
    if (data===undefined){
        console.error("DATA UNDEFINED!")
        return;
    }
    data.autoActive = !data.autoActive;
    browser.storage.local.set({
        "ssGG": data
    });
    updateElements();
}

function loadData(){
    loadCurrentTab();
    browser.storage.local.get("ssGG").then((res)=>{
        console.info(res);
        if (res.ssGG===undefined){
            data = defaultConfig;
        }else data = res.ssGG;
        // alert(data);
        updateElements();
    });
}
function clearData(){
    if (data===undefined)return;
    if (data.active){
        data.active=false;
        saveData();
    }
    setTimeout(()=>{
        if (data.url!=="none"){
            if (steelSeries===undefined) steelSeries = new SteelSeries("http://"+data.url+"/");
            steelSeries.unregisterApp();
        }

        browser.storage.local.remove("ssGG").then(()=>{
            loadData();
        });
    },500);

}
function saveNewData(){
    data.screenSize=document.getElementById("select").value;
    saveData();
}

function saveData(){
    if (data===undefined)return;
    browser.storage.local.set({
        "ssGG": data
    });
}

function updateElements(){
    if (data.autoActive){
        document.getElementById("toggleAutoActive").setAttribute("class","green pointer");
    }else{
        document.getElementById("toggleAutoActive").setAttribute("class","red pointer");
    }
    if (data.active){
        document.getElementById("toggleActive").setAttribute("class","green pointer");
    }else{
        document.getElementById("toggleActive").setAttribute("class","red pointer");
    }
    if (currentTabId===undefined){
        document.getElementById("toggleActiveTab").setAttribute("class","grey");
    }else if (data.disabledTabs.includes("_"+currentTabId)){
        document.getElementById("toggleActiveTab").setAttribute("class","red pointer");
    }else{
        document.getElementById("toggleActiveTab").setAttribute("class","green pointer");
    }
    document.getElementById("select").value = data.screenSize;
}

function newConfig(){
    data.selectNew = true;
    saveData();
}

function updateData(newData) {
    console.log(newData)
    newData = newData.ssGG;
    if (newData.newValue.autoActive !== undefined) {
        data.autoActive = newData.newValue.autoActive
    }
    if (newData.newValue.active !== undefined) {
        data.active = newData.newValue.active
    }
    if (newData.newValue.selectNew !== undefined) {
        data.selectNew = newData.newValue.selectNew;
    }
    if (newData.newValue.url !== undefined) {
        data.url = newData.newValue.url
        if (steelSeries !== undefined) {
            steelSeries.setURL("http://" + data.url + "/")
        }
    }

    data = newData.newValue;
}
function getCurrentWindowTabs() {
    return browser.tabs.query({currentWindow: true});
}


Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function toggleActiveTab(){
    if (currentTabId===undefined)return;
    if (data.disabledTabs.includes("_"+currentTabId)){
        data.disabledTabs.remove("_"+currentTabId);
    }else{
        data.disabledTabs.push("_"+currentTabId);
    }
    saveData();
    browser.tabs.sendMessage(
        currentTabId,
        {disabled: data.disabledTabs.includes("_"+currentTabId)}
    )
    updateElements();
}

function loadCurrentTab(){
    getCurrentWindowTabs().then((tabs)=>{
        console.info(tabs);
        tabs.forEach(tab=>{
            if (tab.active){
                //current tab
                currentTabId=tab.id;
            }
        })
    })
}

loadData();
listenForClicks();
browser.storage.onChanged.addListener(updateData);

