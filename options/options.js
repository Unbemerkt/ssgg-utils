var data;
var defaultConfig = {
    active: true,
    autoActive: true,
    screenSize: "screened-128x40",
    url: "none",
    selectNew: true,
    youTube: {
        active: true,
        states: {
            _n1: {
                friendlyName: "Not Started",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _0: {
                friendlyName: "Ended",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _1: {
                friendlyName: "Playing",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _2: {
                friendlyName: "Paused",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _3: {
                friendlyName: "Buffering",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _5: {
                friendlyName: "Video cued",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _6: {
                friendlyName: "No video selected",
                line1: "No Video",
                line2: "Selected!",
                line2_progress: false
            }
        }

    },
    soundCloud: {
        active: true,
        line1: "%title%",
        line2: "%artist%",
        line2_progress: false
    },
    soundCloudEmbed: {
        active: true,
        paused: {
            line1: "%title%",
            line2: "%paused%",
            line2_progress: false
        },
        line1: "%title%",
        line2: "%playing%",
        line2_progress: false
    },
    wtg: {
        active: true,
        paused: {
            line1: "» %title% «» %artist% «",
            line2: "%paused%",
            showProgress: false
        },
        line1: "» %title% «» %artist% «",
        line2: "%playing%",
        line2_progress: true
    },
    disabledTabs: []
};

function saveOptions(e) {
    e.preventDefault();
    data.autoActive = document.getElementById("automatic").checked;
    data.active = document.getElementById("activate").checked;
    data.screenSize = document.getElementById("select").value;


    //youtube
    var ytState = document.getElementById("yt_state").value;
    data.youTube.active = document.getElementById("yt_active").checked;
    data.youTube.states[ytState].line1 = document.getElementById("yt_first").value;
    data.youTube.states[ytState].line2 = document.getElementById("yt_second").value;
    data.youTube.states[ytState].line2_progress = document.getElementById("yt_progress").checked;

    //soundCloud
    data.soundCloud.active = document.getElementById("sC_active").checked;
    data.soundCloud.line1 = document.getElementById("sC_first").value;
    data.soundCloud.line2 = document.getElementById("sC_second").value;
    data.soundCloud.line2_progress = document.getElementById("sC_progress").checked;


    //soundCloudEmbed
    data.soundCloudEmbed.active = document.getElementById("sCE_active").checked;
    data.soundCloudEmbed.line1 = document.getElementById("sCE_first").value;
    data.soundCloudEmbed.line2 = document.getElementById("sCE_second").value;
    data.soundCloudEmbed.paused.line1 = document.getElementById("sCE_paused_first").value;
    data.soundCloudEmbed.paused.line2 = document.getElementById("sCE_paused_second").value;


    //wtg
    data.wtg.active = document.getElementById("wtg_active").checked;
    data.wtg.line1 = document.getElementById("wtg_first").value;
    data.wtg.line2 = document.getElementById("wtg_second").value;
    data.wtg.line2_progress = document.getElementById("wtg_progress").checked;
    data.wtg.paused.line1 = document.getElementById("wtg_paused_first").value;
    data.wtg.paused.line2 = document.getElementById("wtg_paused_second").value;
    data.wtg.paused.showProgress = document.getElementById("wtg_paused_progress").checked;



    saveData();
}

function loadOptions() {
    browser.storage.local.get("ssGG").then((res) => {
        if (res.ssGG === undefined) {
            data = defaultConfig;
        } else data = res.ssGG;
        updateElements();
    });
}

function updateElements() {
    document.getElementById("yt_state").removeEventListener("change",ytStateChange);
    document.getElementById("select").value = data.screenSize;
    document.getElementById("automatic").checked = data.autoActive;
    document.getElementById("activate").checked = data.active;

    //youtube
    document.getElementById("yt_active").checked = data.youTube.active;
    document.getElementById("yt_first").value = data.youTube.states["_1"].line1;
    document.getElementById("yt_second").value = data.youTube.states["_1"].line2;
    document.getElementById("yt_progress").checked = data.youTube.states["_1"].line2_progress;
    document.getElementById("yt_state").addEventListener("change",ytStateChange);
    //soundCloud
    document.getElementById("sC_active").checked = data.soundCloud.active;
    document.getElementById("sC_first").value = data.soundCloud.line1;
    document.getElementById("sC_second").value = data.soundCloud.line2;
    document.getElementById("sC_progress").checked = data.soundCloud.line2_progress;
    //soundCloudEmbed
    document.getElementById("sCE_active").checked = data.soundCloudEmbed.active;
    document.getElementById("sCE_first").value = data.soundCloudEmbed.line1;
    document.getElementById("sCE_second").value = data.soundCloudEmbed.line2;
    document.getElementById("sCE_paused_first").value = data.soundCloudEmbed.paused.line1;
    document.getElementById("sCE_paused_second").value = data.soundCloudEmbed.paused.line2;

    //wtg
    document.getElementById("wtg_active").checked = data.wtg.active;
    document.getElementById("wtg_first").value = data.wtg.line1;
    document.getElementById("wtg_second").value = data.wtg.line2;
    document.getElementById("wtg_progress").checked = data.wtg.line2_progress;
    document.getElementById("wtg_paused_first").value = data.wtg.paused.line1;
    document.getElementById("wtg_paused_second").value = data.wtg.paused.line2;
    document.getElementById("wtg_paused_progress").checked = data.wtg.paused.showProgress;


}

function ytStateChange(){
    var val = document.getElementById("yt_state").value;
    document.getElementById("yt_first").value = data.youTube.states[val].line1;
    document.getElementById("yt_second").value = data.youTube.states[val].line2;
    document.getElementById("yt_progress").checked = data.youTube.states[val].line2_progress;
}

function saveData() {
    if (data === undefined) return;
    browser.storage.local.set({
        "ssGG": data
    });
}

function updateData(newData) {
    data = newData.ssGG.newValue
    updateElements();
}

document.addEventListener("DOMContentLoaded", loadOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
browser.storage.onChanged.addListener(updateData);