class SteelSeries {
    constructor(url) {
        this.url = url;
        this.name="SSGG_WEB_UTILS"
    }

    setScreen(value) {
        this.screen = value;
    }

    getScreen() {
        return this.screen;
    }

    setURL(url) {
        this.url = url;
    }

    setText(line1, line2) {
        this.sendData("game_event", {
            "game": this.name,
            "event": "SC_IDLE_STATE",
            "data": {
                "value": Math.floor(Math.random() * Math.floor(99)),
                "frame": {
                    "first-line": line1,
                    "second-line": line2,
                }
            }
        });
    }

    setProgress(line1, progress) {
        this.sendData("game_event", {
            "game": this.name,
            "event": "SC_PROGRESS_UPDATE",
            "data": {
                "value": Math.floor(Math.random() * Math.floor(99)),
                "frame": {
                    "first-line": line1,
                    "progress-value": progress
                }
            }
        });
    }

    async sendData(endPoint, obj) {

// Default options are marked with *
        const response = await fetch(this.url + endPoint, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            // mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(obj) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

    registerApp() {
        this.sendData("game_metadata", {
            "game": this.name,
            "game_display_name": "SteelSeriesGG WebUtils",
            "developer": "Unbemerkt"
        });
        this.sendData("bind_game_event", {
            "game": this.name,
            "event": "SC_PROGRESS_UPDATE",
            "handlers": [
                {
                    "device-type": this.screen,
                    "zone": "one",
                    "mode": "screen",
                    "datas": [
                        {
                            "lines": [
                                {
                                    "has-text": true,
                                    "context-frame-key": "first-line",
                                },
                                {
                                    "has-progress-bar": true,
                                    "context-frame-key": "progress-value"
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        this.sendData("bind_game_event", {
            "game": this.name,
            "event": "SC_IDLE_STATE",
            "handlers": [
                {
                    "device-type": this.screen,
                    "zone": "one",
                    "mode": "screen",
                    "datas": [
                        {
                            "lines": [
                                {
                                    "has-text": true,
                                    "context-frame-key": "first-line",
                                },
                                {
                                    "has-text": true,
                                    "has-progress-bar": false,
                                    "context-frame-key": "second-line"
                                }
                            ]
                        }
                    ]
                }
            ]
        })
    }

    unregisterApp() {
        this.sendData("remove_game", {"game": "SSGG_WEB_UTILS"})
    }
}