class Watch2Gether {
    constructor(config, steelSeries) {
        if (config === undefined || steelSeries === undefined) return;
        console.info("[SS-GG Utils] loading Watch2Gether module for " + window.location.href)
        this.config = config;
        this.steelSeries = steelSeries;
        this.injectScripts();
        this.messageListener();
    }

    validatePage() {
        return document.location.href.toLowerCase().replaceAll("https://", "")
            .replaceAll("http://", "")
            .startsWith("player.w2g.tv");
    }

    start() {
        if (this.timer === undefined) {
            this.timer = setTimeout(() => {
                this.update(this)
            }, 2000);
        }
    }

    stop(bool) {
        if (this.timer !== undefined) {
            clearTimeout(this.timer);
            this.timer = undefined;
            if (bool) {
                this.steelSeries.setText("Module Deactivated", "Watch2Gether")
            } else {
                this.steelSeries.setText("Bye Bye!", "See you Later!")
            }
        }
    }

    isActive() {
        return this.config.wtg.active;
    }

    updateConfig(cfg, overrideCfg) {
        this.config = cfg;
        this.overrideCfg = overrideCfg;
        if (overrideCfg.use) {
            if (overrideCfg.active) {
                this.start();
            } else {
                this.stop();
            }
        } else {
            if (this.isActive() && this.config.active) {
                this.start();
            } else {
                this.stop(!this.isActive());
            }
        }
    }

    songData = {
        title: "Loading...",
        artist: "Loading...",
    }

    update(sc) {
        if (sc.timer === undefined) return;
        try {
            const timeDisplay = document.getElementById("time_display");
            const playBTN = document.getElementById("player_controls").children[0].children[0];
            const iframe = document.getElementsByTagName("iframe")[0];
            if (iframe === null || iframe === undefined) {
                //iframe not loaded
                sc.steelSeries.setText(getString("_0", "Loading..."), getString("_1", sc.config.wtg.paused.line2
                    .replace(/%playing%/g, " » Playing").replace(/%paused%/g, " » Paused")));
                sc.timer = setTimeout(() => {
                    sc.update(sc);
                }, 2000);
                return;
            }

            if (iframe.src.includes("www.youtube.com/embed")) {
                this.songData.title = iframe.title;
            } else if (iframe.src.includes("w.soundcloud.com/player")) {
                //do almost nothing...
                if (this.songData.title === "") this.songData.title = "Loading...";
            } else {
                this.songData.title = "Title not found!";
            }


            if (playBTN === null || playBTN === undefined || playBTN.className === "icon-play") {
                //paused
                if (sc.config.wtg.paused.showProgress) {
                    sc.steelSeries.setProgress(getString("_0", sc.config.wtg.paused.line1
                            .replace(/%playing%/g, " » Playing")
                            .replace(/%title%/g, this.songData.title)
                            .replace(/%artist%/g, this.songData.artist)),
                        Math.floor((this.calcProgress(timeDisplay) / this.calcDuration(timeDisplay)) * 100))
                } else {
                    sc.steelSeries.setText(getString("_0", sc.config.wtg.paused.line1.replace(/%paused%/g, " » Paused").replace(/%playing%/g, " » Playing")
                        .replace(/%artist%/g, this.songData.artist)
                        .replace(/%title%/g, this.songData.title)), getString("_1", sc.config.wtg.paused.line2.replace(/%paused%/g, " » Paused").replace(/%playing%/g, " » Playing")
                        .replace(/%title%/g, this.songData.title).replace(/%author%/g, this.songData.artist)))
                }
                sc.timer = setTimeout(() => {
                    sc.update(sc);
                }, 2000);
                return;
            }


            if (sc.config.wtg.line2_progress) {
                //calc progress
                sc.steelSeries.setProgress(getString("_0", sc.config.wtg.line1
                        .replace(/%title%/g, this.songData.title).replace(/%playing%/g, " » Playing")
                        .replace(/%artist%/g, this.songData.artist)),
                    Math.floor((this.calcProgress(timeDisplay) / this.calcDuration(timeDisplay)) * 100))
            } else {
                sc.steelSeries.setText(getString("_0", sc.config.wtg.line1.replace(/%playing%/g, " » Playing")
                        .replace(/%title%/g, this.songData.title).replace(/%artist%/g, this.songData.artist)),
                    getString("_1", sc.config.wtg.line2.replace(/%artist%/g, this.songData.artist)
                        .replace(/%playing%/g, " » Playing")
                        .replace(/%title%/g, this.songData.title)))
            }
            //state: document.getElementsByClassName("playControls__elements")[0].children[1].classList.contains("playing")
        } catch (e) {
            console.error("SS-GG Utils (SoundCloud):", e);
            sc.steelSeries.setText("An error occurred!", getString("_1", "Press F12 > Console!"))
        }
        sc.timer = setTimeout(() => {
            sc.update(sc);
        }, 2000);
    }

    calcProgress(timeDisplay) {
        const currentTime = timeDisplay.childNodes[0].innerText.split(":");
        var progress = 0;
        if (currentTime.length === 3) {
            //calc with hours
            const h = parseInt(currentTime[0]) * 60 * 60;
            const m = parseInt(currentTime[1]) * 60;
            const s = parseInt(currentTime[2]);
            progress = h + m + s;
        } else if (currentTime.length === 2) {
            //calc with minutes
            const m = parseInt(currentTime[0]) * 60;
            const s = parseInt(currentTime[1]);
            progress = m + s;
        } else {
            progress = parseInt(currentTime[0]);
        }
        return progress;
    }

    calcDuration(timeDisplay) {
        const durationTime = timeDisplay.childNodes[2].innerText.split(":");

        //calc Time
        var duration = 1;

        if (durationTime.length === 3) {
            //calc with hours
            const h = parseInt(durationTime[0]) * 60 * 60;
            const m = parseInt(durationTime[1]) * 60;
            const s = parseInt(durationTime[2]);
            duration = h + m + s;
        } else if (durationTime.length === 2) {
            //calc with minutes
            const m = parseInt(durationTime[0]) * 60;
            const s = parseInt(durationTime[1]);
            duration = m + s;
        } else {
            duration = parseInt(durationTime[0]);
        }
        return duration;
    }

    injectScripts() {
        console.info("[SS-GG Utils] Injecting Scripts...")
        const scriptElm = document.createElement("script");
        scriptElm.id = "ssGGMediaLoader"
        scriptElm.src = chrome.runtime.getURL('injectScripts/wtgDataLoader.js');
        document.getElementsByTagName("body")[0].appendChild(scriptElm);
    }

    messageListener() {
        window.addEventListener("message", (event) => {
            if (event.data.TYPE && (event.data.TYPE === "UPDATE_DATA")) {
                if (event.data.NEW_DATA.title !== undefined) this.songData.title = event.data.NEW_DATA.title;
                if (event.data.NEW_DATA.artist !== undefined) this.songData.artist = event.data.NEW_DATA.artist;
            }
        })
    }

}
