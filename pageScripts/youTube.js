class YouTube {
    constructor(config,steelSeries) {
        if (config===undefined||steelSeries===undefined)return;
        console.info("SS-GG Utils: loading YouTube module for "+window.location.href)
        this.config = config;
        this.steelSeries = steelSeries;
    }

    validatePage(){
        return document.location.href.toLowerCase().replaceAll("https://","")
            .replaceAll("http://","")
            .startsWith("www.youtube.com")
    }

    start(){
        if (this.timer ===undefined){
            this.timer = setTimeout(()=>{
                this.update(this)
            },2000);
        }
    }

    stop(bool){
        if (this.timer!==undefined){
            clearTimeout(this.timer);
            this.timer=undefined;
            if (bool){
                this.steelSeries.setText("Module Deactivated","YouTube")
            }else{
                this.steelSeries.setText("Bye Bye!","See you Later!")
            }

        }
    }

    isActive(){
        return this.config.youTube.active;
    }

    updateConfig(cfg, overrideCfg){
        this.config=cfg;
        this.overrideCfg=overrideCfg;
        if (overrideCfg.use){
            if (overrideCfg.active){
                this.start();
            } else {
                this.stop();
            }
        }else {
            if (this.isActive() && this.config.active) {
                this.start();
            } else {
                this.stop(!this.isActive());
            }
        }
    }

    update(yt){
        if (yt.timer===undefined)return;
        try {
            if (!window.location.href.includes("/watch?v=")&&!window.location.href.includes("/embed")) {
                yt.player = undefined;
                yt.steelSeries.setText(getString("_0",yt.config.youTube.states._6.line1), getString("_1",yt.config.youTube.states._6.line2));
                yt.timer = setTimeout(()=>{
                    yt.update(yt)
                }, 2000);
                return;
            }
            if (yt.player === undefined) {
                yt.player = document.getElementById("movie_player").wrappedJSObject;
            }
            if (yt.player.getPlayerState()>=-1&&yt.player.getPlayerState()<=5){
                var pDat;
                if (yt.player.getPlayerState()===-1){
                    pDat = yt.config.youTube.states._n1;
                }else{
                    pDat = yt.config.youTube.states["_"+yt.player.getPlayerState()];
                }

                if (pDat.line2_progress){
                    yt.steelSeries.setProgress(getString("_0",pDat.line1
                        .replace(/%title%/g,yt.player.getVideoData().title)
                        .replace(/%artist%/g,yt.player.getVideoData().artist)), Math.floor((yt.player.getCurrentTime() / yt.player.getDuration()) * 100));
                }else{
                    yt.steelSeries.setText(getString("_0",pDat.line1
                        .replace(/%title%/g,yt.player.getVideoData().title)
                        .replace(/%artist%/g,yt.player.getVideoData().artist)), getString("_1",pDat.line2
                        .replace(/%title%/g,yt.player.getVideoData().title)
                        .replace(/%artist%/g,yt.player.getVideoData().artist)));
                }

            }else{
                yt.steelSeries.setText(getString("_0",yt.config.youTube.states._6.line1), getString("_1",yt.config.youTube.states._6.line2));
            }
        }catch (e){
            console.error("SS-GG Utils (YouTube):",e);
            yt.steelSeries.setText(getString("_0"), getString("_1","Press F12 > Console!"))
        }
        yt.timer = setTimeout(()=>{
            yt.update(yt)
        }, 2000);
    }
}