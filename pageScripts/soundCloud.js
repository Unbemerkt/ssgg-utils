class SoundCloud {
    constructor(config,steelSeries) {
        if (config===undefined||steelSeries===undefined)return;
        console.info("SS-GG Utils: loading SoundCloud module for "+window.location.href)
        this.config = config;
        this.steelSeries = steelSeries;
    }

    validatePage(){
        return document.location.href.toLowerCase().replaceAll("https://","")
            .replaceAll("http://","")
            .startsWith("soundcloud.com")
    }

    start(){
        if (this.timer ===undefined){
            this.timer = setTimeout(()=>{
                this.update(this)
            },2000);
        }
    }

    stop(bool){
        if (this.timer!==undefined){
            clearTimeout(this.timer);
            this.timer=undefined;
            if (bool){
                this.steelSeries.setText("Module Deactivated","SoundCloud")
            }else{
                this.steelSeries.setText("Bye Bye!","See you Later!")
            }
        }
    }

    isActive(){
        return this.config.soundCloud.active;
    }

    updateConfig(cfg, overrideCfg){
        this.config=cfg;
        this.overrideCfg=overrideCfg;
        if (overrideCfg.use){
            if (overrideCfg.active){
                this.start();
            } else {
                this.stop();
            }
        }else {
            if (this.isActive() && this.config.active) {
                this.start();
            } else {
                this.stop(!this.isActive());
            }
        }
    }

    update(sc){
        if (sc.timer===undefined)return;
        try {
            var obj = document.getElementsByClassName("playbackSoundBadge__titleContextContainer");
            if (obj === undefined||obj.length===0||obj[0].children===undefined||obj[0].children.length===0) {
                sc.steelSeries.setText("Title not found!","296")
                sc.timer = setTimeout(()=>{
                    sc.update(sc);
                }, 2000);
                return;
            }
            var title = obj[0].children[1].children[0].children[1].innerHTML;
            var artist = obj[0].children[0].innerHTML;
            if (sc.config.soundCloud.line2_progress){
                sc.steelSeries.setProgress(getString("_0",sc.config.soundCloud.line1.replace(/%artist%/g,artist)
                        .replace(/%title%/g,title)),
                    parseInt(document.getElementsByClassName("playbackTimeline__progressBar")[0].style.width.replace("%","")))
            }else{
                sc.steelSeries.setText(getString("_0",sc.config.soundCloud.line1.replace(/%artist%/g,artist)
                    .replace(/%title%/g,title)),getString("_1",sc.config.soundCloud.line2.replace(/%artist%/g,artist)
                    .replace(/%title%/g,title)))
            }
            //state: document.getElementsByClassName("playControls__elements")[0].children[1].classList.contains("playing")
        }catch (e){
            console.error("SS-GG Utils (SoundCloud):",e);
            sc.steelSeries.setText("An error occurred!", getString("_1","Press F12 > Console!"))
        }
        sc.timer = setTimeout(()=>{
            sc.update(sc);
        }, 2000);
    }
}