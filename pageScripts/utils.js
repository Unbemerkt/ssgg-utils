var lastString = {};
const maxLength = 18;
function getString(id,string) {
    //console.log("In: ",string)
    if (lastString[id]===undefined||string !== lastString[id].string) {
        lastString[id] = {string,index:0};
    }
    if (string.split('').length<=maxLength){
        return string;
    }
    string = string.split('');
    var res = "";
    if (lastString[id].index >= (string.length)) {
        lastString[id].index = 0;
    }
    var i4 = 0;
    for (var i3 = 0; i3 < maxLength && i3 < string.length; i3++) {
        if (string[i3 + lastString[id].index] !== undefined) {
            res += string[i3 + lastString[id].index];
            i4++;
        }
    }
    res += "   ";
    for (var i5 = 0; i5 < 15 - i4; i5++) {
        if (string[i5] !== undefined) {
            res += string[i5];
        }
    }

    lastString[id].index+=1;
    return res;
}