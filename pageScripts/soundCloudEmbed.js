class SoundCloudEmbed {
    constructor(config,steelSeries) {
        if (config===undefined||steelSeries===undefined)return;
        console.info("SS-GG Utils: loading SoundCloudEmbed module for "+window.location.href)
        this.config = config;
        this.steelSeries = steelSeries;
    }

    validatePage(){
        return document.location.href.toLowerCase().replaceAll("https://","")
            .replaceAll("http://","")
            .startsWith("w.soundcloud.com/player/?url=")
    }

    start() {
        if (this.timer === undefined) {
            this.timer = setTimeout(() => {
                this.update(this)
            }, 2000);
        }
    }

    stop(bool) {
        if (this.timer !== undefined) {
            clearTimeout(this.timer);
            this.timer = undefined;
            if (bool) {
                this.steelSeries.setText("Module Deactivated", "SoundCloudEmbed")
            } else {
                this.steelSeries.setText("Bye Bye!", "See you Later!")
            }
        }
    }

    isActive() {
        return this.config.soundCloudEmbed.active;
    }

    updateConfig(cfg, overrideCfg) {
        this.config = cfg;
        this.overrideCfg = overrideCfg;
        if (overrideCfg.use) {
            if (overrideCfg.active) {
                this.start();
            } else {
                this.stop();
            }
        } else {
            if (this.isActive() && this.config.active) {
                this.start();
            } else {
                this.stop(!this.isActive());
            }
        }
    }

    update(sc) {
        if (sc.timer === undefined) return;
        try {
            var obj = document.getElementsByClassName("title__h2 sc-link-dark sc-truncate g-text-shadow  title__line");
            if (obj === null || obj === undefined || obj.length === 0) {
                sc.steelSeries.setText("Title not found!", " » Paused")
                sc.timer = setTimeout(() => {
                    sc.update(sc);
                }, 2000);
                return;
            }
            const playBTN = document.getElementsByClassName("playButton medium playing");
            const title = obj[0].title;
            if (playBTN===null||playBTN===undefined||playBTN.length < 1) {
                //paused
                sc.steelSeries.setText(getString("_0", sc.config.soundCloudEmbed.paused.line1.replace(/%paused%/g, " » Paused")
                    .replace(/%title%/g, title)), getString("_1", sc.config.soundCloudEmbed.paused.line2.replace(/%paused%/g, " » Paused")
                    .replace(/%title%/g, title)))
                sc.timer = setTimeout(() => {
                    sc.update(sc);
                }, 2000);
                return;
            }

            sc.steelSeries.setText(getString("_0", sc.config.soundCloudEmbed.line1.replace(/%playing%/g, " » Playing")
                .replace(/%title%/g, title)), getString("_1", sc.config.soundCloudEmbed.line2.replace(/%playing%/g, " » Playing")
                .replace(/%title%/g, title)))
            //state: document.getElementsByClassName("playControls__elements")[0].children[1].classList.contains("playing")
        } catch (e) {
            console.error("SS-GG Utils (soundCloudEmbed):", e);
            sc.steelSeries.setText("An error occurred!", getString("_1", "Press F12 > Console!"))
        }
        sc.timer = setTimeout(() => {
            sc.update(sc);
        }, 2000);
    }
}