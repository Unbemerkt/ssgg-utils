//https://extensionworkshop.com/documentation/publish/submitting-an-add-on/
var data;
var defaultConfig = {
    active: true,
    autoActive: true,
    screenSize: "screened-128x40",
    url: "none",
    selectNew: true,
    youTube: {
        active: true,
        states: {
            _n1: {
                friendlyName: "Not Started",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _0: {
                friendlyName: "Ended",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _1: {
                friendlyName: "Playing",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _2: {
                friendlyName: "Paused",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _3: {
                friendlyName: "Buffering",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _5: {
                friendlyName: "Video cued",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _6: {
                friendlyName: "No video selected",
                line1: "No Video",
                line2: "Selected!",
                line2_progress: false
            }
        }

    },
    soundCloud: {
        active: true,
        line1: "%title%",
        line2: "%artist%",
        line2_progress: false
    },
    soundCloudEmbed: {
        active: true,
        paused: {
            line1: "%title%",
            line2: "%paused%",
            line2_progress: false
        },
        line1: "%title%",
        line2: "%playing%",
        line2_progress: false
    },
    wtg: {
        active: true,
        paused: {
            line1: "» %title% «» %artist% «",
            line2: "%paused%",
            showProgress: false
        },
        line1: "» %title% «» %artist% «",
        line2: "%playing%",
        line2_progress: true
    },
    disabledTabs: []
};
var steelSeries;
var loadedModule;
var overrideIsActive = {
    use: false,
    active: true
};

function updateData(newData) {
    newData = newData.ssGG;
    if (newData.newValue.autoActive !== undefined) {
        data.autoActive = newData.newValue.autoActive
    }
    if (newData.newValue.active !== undefined) {
        data.active = newData.newValue.active
        if (overrideIsActive.use){
            if (overrideIsActive.active){
                if (loadedModule!==undefined)loadedModule.start();
            } else {
                if (loadedModule!==undefined)loadedModule.stop();
            }
        }else{
            if (data.active) {
                if (loadedModule!==undefined)loadedModule.start();
            } else {
                if (loadedModule!==undefined)loadedModule.stop();
            }
        }
    }
    if (newData.newValue.selectNew !== undefined) {
        if (newData.newValue.selectNew){
            addSelector();
        }else{
            removeSelector();
        }

    }
    if (newData.newValue.url !== undefined) {
        data.url = newData.newValue.url
        if (steelSeries !== undefined) {
            steelSeries.setURL("http://" + data.url + "/")
        }
    }
    data = newData.newValue;
    if (loadedModule!==undefined)loadedModule.updateConfig(data, overrideIsActive);
}

function saveData() {
    if (data === undefined) return;
    browser.storage.local.set({
        "ssGG": data
    });
}

function addSelector(){
    var div = document.createElement("div");
    div.setAttribute("style","z-index: 999999; position: absolute; padding: 1em; background: #ffffff; top: 0; left: 0;")
    div.id="SSGG_div";
    var input = document.createElement("input");
    input.id="SSGG_selector";
    input.setAttribute("type","file");
    var h = document.createElement("h4");
    h.innerHTML="Select coreProps.json"
    var btn = document.createElement("a");
    btn.innerHTML="x";
    btn.setAttribute("onclick",'document.getElementById("SSGG_div").remove()')
    div.appendChild(btn)
    div.appendChild(h)
    div.appendChild(input);
    document.getElementsByTagName("body")[0].appendChild(div);
    input.addEventListener("change",()=>{
        var fr=new FileReader();
        fr.onload=function(){
            data.url = JSON.parse(fr.result).address;
            data.selectNew=false;
            saveData();
            setTimeout(init,1000);
        }
        fr.readAsText(input.files[0]);
    });
}

function removeSelector(){
    var div = document.getElementById("SSGG_div");
    if (div===undefined||div===null)return;
    div.remove()
}

function init() {
    try {
        browser.storage.local.get("ssGG").then((res) => {
            if (res.ssGG === undefined) {
                data = defaultConfig;
            } else data = res.ssGG;
            // console.info(data);
            if (data.selectNew) {
                addSelector();
                return;
            }
            removeSelector()
            //init steelSeries
            steelSeries = new SteelSeries("http://" + data.url + "/");
            steelSeries.setScreen(data.screenSize);
            steelSeries.registerApp();

            //youtube
            if (new YouTube().validatePage()) {
                loadedModule = new YouTube(data, steelSeries);
                if (!loadedModule.isActive()) {
                    console.info("SS-GG Utils: YouTube module is deactivated!")
                    return;
                }
            }

            //soundCloud
            if (new SoundCloud().validatePage()) {
                loadedModule = new SoundCloud(data, steelSeries);
                if (!loadedModule.isActive()) {
                    console.info("SS-GG Utils: SoundCloud module is deactivated!")
                    return;
                }
            }

            //SoundCloudEmbed
            if (new SoundCloudEmbed().validatePage()) {
                loadedModule = new SoundCloudEmbed(data, steelSeries);
                if (!loadedModule.isActive()) {
                    console.info("SS-GG Utils: SoundCloudEmbed module is deactivated!")
                    return;
                }
            }

            //w2g.tv
            if (new Watch2Gether().validatePage()) {
                console.log("[SS-GG Utils] Loaded module!")
                loadedModule = new Watch2Gether(data, steelSeries);
                if (!loadedModule.isActive()) {
                    console.info("SS-GG Utils: Watch2Gether module is deactivated!")
                    return;
                }
            }


            if (loadedModule===undefined){
                console.info("SS-GG Utils: No module found for "+window.location.href);
                return;
            }
            //listen for browser messages
            messageListener();
            //register in SS-GG
            setTimeout(() => {
                if (data.autoActive) {
                    if (!data.active) {
                        data.active = true;
                        saveData();
                    }
                    loadedModule.start();
                }
                browser.storage.onChanged.addListener(updateData);
            }, 500);
        });
    }catch (e){
        console.error(e);
    }
}

function messageListener(){
    browser.runtime.onMessage.addListener(request => {
        if (request.disabled){
            overrideIsActive.use=true;
            overrideIsActive.active=false;
            if (loadedModule!==undefined)loadedModule.stop();
        }else{
            overrideIsActive.use=false;
            if (data.active){
                if (loadedModule!==undefined)loadedModule.start();
            }
        }
    });
}

init();


