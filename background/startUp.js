var data;
var defaultConfig = {
    active: true,
    autoActive: true,
    screenSize: "screened-128x40",
    url: "none",
    selectNew: true,
    youTube: {
        active: true,
        states: {
            _n1: {
                friendlyName: "Not Started",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _0: {
                friendlyName: "Ended",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _1: {
                friendlyName: "Playing",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _2: {
                friendlyName: "Paused",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _3: {
                friendlyName: "Buffering",
                line1: "%title%",
                line2: "%artist%",
                line2_progress: true
            },
            _5: {
                friendlyName: "Video cued",
                line1: "%title%",
                line2: "  » Paused",
                line2_progress: false
            },
            _6: {
                friendlyName: "No video selected",
                line1: "No Video",
                line2: "Selected!",
                line2_progress: false
            }
        }

    },
    soundCloud: {
        active: true,
        line1: "%title%",
        line2: "%artist%",
        line2_progress: false
    },
    soundCloudEmbed: {
        active: true,
        paused: {
            line1: "%title%",
            line2: "%paused%",
            line2_progress: false
        },
        line1: "%title%",
        line2: "%playing%",
        line2_progress: false
    },
    wtg: {
        active: true,
        paused: {
            line1: "» %title% «» %artist% «",
            line2: "%paused%",
            showProgress: false
        },
        line1: "» %title% «» %artist% «",
        line2: "%playing%",
        line2_progress: true
    },
    disabledTabs: []
};


Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function loadData() {
    browser.storage.local.get("ssGG").then((res) => {
        if (res.ssGG === undefined) {
            data = defaultConfig;
        } else data = res.ssGG;
        handleStart();
    });
}

function saveData() {
    if (data === undefined) return;
    browser.storage.local.set({
        "ssGG": data
    });
}

function handleStart() {
    if (data === undefined) return;
    data.disabledTabs = [];
    if (data.autoActive && !data.active) {
        data.active = true;
    }
    saveData();
}

loadData();
function removeTab(tabId){
    browser.storage.local.get("ssGG").then((res) => {
        if (res.ssGG === undefined) {
            data = defaultConfig;
        } else data = res.ssGG;
        if (data.disabledTabs.includes("_"+tabId)){
            data.disabledTabs.remove("_"+tabId);
            saveData();
        }
    });
}

browser.tabs.onRemoved.addListener(removeTab)



// function updateData(newData) {
//     console.log(newData)
//     console.log()

// }
// browser.storage.onChanged.addListener(updateData);